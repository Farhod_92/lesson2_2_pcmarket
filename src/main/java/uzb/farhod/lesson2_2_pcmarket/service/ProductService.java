package uzb.farhod.lesson2_2_pcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uzb.farhod.lesson2_2_pcmarket.entity.*;
import uzb.farhod.lesson2_2_pcmarket.entity.Character;
import uzb.farhod.lesson2_2_pcmarket.payload.ApiResponse;
import uzb.farhod.lesson2_2_pcmarket.payload.ProductDto;
import uzb.farhod.lesson2_2_pcmarket.payload.ProductFilterDto;
import uzb.farhod.lesson2_2_pcmarket.repository.*;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRestRepository categoryRestRepository;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    CharacterRestRepository characterRestRepository;


    public ApiResponse add(ProductDto productDto){
        Optional<Category> optionalCategory = categoryRestRepository.findById(productDto.getCategoryId());
        if(!optionalCategory.isPresent())
            return new ApiResponse("categoriya topilmadi", false);

        try{
            Product product = new Product();
            product.setCategory(optionalCategory.get());
            product.setName(productDto.getName());
            product.setPrice(productDto.getPrice());
            product.setWarranty(productDto.getWarranty());
            product.setInStock(productDto.isInStock());
            product.setSaleOnlyInComplete(productDto.isSaleOnlyInComplete());
            productRepository.save(product);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }

        return new ApiResponse("product qo'shildi", true);
    }

    public List<Product> getAll(){
        return productRepository.findAll() ;
    }

    public Product getById(Integer id){
        return productRepository.findById(id).orElse(null) ;
    }

    public ApiResponse edit(Integer id, ProductDto productDto){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if(!optionalProduct.isPresent())
            return new ApiResponse("product topilmadi", false);

        Optional<Category> optionalCategory = categoryRestRepository.findById(productDto.getCategoryId());
        if(!optionalCategory.isPresent())
            return new ApiResponse("categoriya topilmadi", false);

        try{
            Product product = optionalProduct.get();
            product.setCategory(optionalCategory.get());
            product.setName(productDto.getName());
            product.setPrice(productDto.getPrice());
            product.setWarranty(productDto.getWarranty());
            product.setInStock(productDto.isInStock());
            product.setSaleOnlyInComplete(productDto.isSaleOnlyInComplete());
            productRepository.save(product);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }

        return new ApiResponse("product tahrirlandi", true);
    }

    public ApiResponse delete(Integer id){
        try {
            productRepository.deleteById(id);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }
        return new ApiResponse("product o'chirildi", true);
    }
    /** Product rasmlarini boshqarish*/

    public ApiResponse addPhoto(Integer id, MultipartFile photo){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if(!optionalProduct.isPresent())
            return new ApiResponse("product topilmadi", false);

        try{
            Attachment attachment = attachmentService.add(photo);
            Product product = optionalProduct.get();
            List<Attachment> photos = product.getPhotos();
            photos.add(attachment);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }

        return new ApiResponse("productga rasm qo'shildi", true);
    }

    public ApiResponse deletePhoto(Integer productId, Integer attachmentId){
        Optional<Attachment> attachmentOptional = attachmentRepository.findById(attachmentId);
        if(!attachmentOptional.isPresent())
            return new ApiResponse("photo topilmadi", false);

        Attachment attachment=attachmentOptional.get();

        Optional<Product> optionalProduct = productRepository.findById(productId);
        if(!optionalProduct.isPresent())
            return new ApiResponse("product topilmadi", false);

        Product product = optionalProduct.get();
        List<Attachment> photos = product.getPhotos();
        photos.remove(attachment);

        boolean delete = attachmentService.delete(attachment);

        if(!delete)
        return new ApiResponse("product rasm o'chirilmadi", false);

        return new ApiResponse("product rasm o'chirildi", true);
    }

    /** Product characterlarini boshqarish*/
    public ApiResponse addCharacter(Integer productId, String characterName){
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if(!optionalProduct.isPresent())
            return new ApiResponse("product topilmadi", false);

        Optional<Character> optionalCharacter = characterRestRepository.findByName(characterName);
        Character character=new Character();
        if(optionalCharacter.isPresent())
            character=optionalCharacter.get();

        character.setName(characterName);
       // characterRestRepository.save(character);

        Product product=optionalProduct.get();
        List<Character> characters = product.getCharacters();
        for (Character character1 : characters) {
            if(character1.equals(character))
                return new ApiResponse("productning bunaqa characteri bor", false);
        }

        Character savedCharacter = characterRestRepository.save(character);
        characters.add(savedCharacter);
        productRepository.save(product);

        return new ApiResponse("productga character qo'shildi", true);
    }

    public List<Character> getCharacters(Integer productId){
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if(!optionalProduct.isPresent())
            return null;

        Product product=optionalProduct.get();
        List<Character> characters = product.getCharacters();

        return characters;
    }

    public ApiResponse deleteCharacter(Integer productId, Integer characterId){
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if(!optionalProduct.isPresent())
            return new ApiResponse("product topilmadi", false);

        Optional<Character> optionalCharacter = characterRestRepository.findById(characterId);
        if(!optionalProduct.isPresent())
            return new ApiResponse("character topilmadi", false);

        try {
            Character character = optionalCharacter.get();

            Product product = optionalProduct.get();
            product.getCharacters().remove(character);

            characterRestRepository.delete(character);
        }catch (Exception e){
             return new ApiResponse("xatolik", false);
        }

        return new ApiResponse("product character o'chirildi", true);
    }

    public List<Product> getByFilter(ProductFilterDto productFilterDto) {
        float minPrice=productFilterDto.getMinPrice();
        float maxPrice=productFilterDto.getMaxPrice();
        List<Integer> propertyIds = productFilterDto.getPropertyIds();

        List<Product>  filteredProducts=productRepository.findAllByFilter(propertyIds,minPrice,maxPrice);
        return filteredProducts;
    }
}