package uzb.farhod.lesson2_2_pcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson2_2_pcmarket.entity.Basket;
import uzb.farhod.lesson2_2_pcmarket.entity.BasketProduct;
import uzb.farhod.lesson2_2_pcmarket.entity.Product;
import uzb.farhod.lesson2_2_pcmarket.payload.ApiResponse;
import uzb.farhod.lesson2_2_pcmarket.payload.BasketDto;
import uzb.farhod.lesson2_2_pcmarket.payload.BasketResponse;
import uzb.farhod.lesson2_2_pcmarket.repository.BasketProductRepository;
import uzb.farhod.lesson2_2_pcmarket.repository.BasketRepository;
import uzb.farhod.lesson2_2_pcmarket.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BasketService {
    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BasketProductRepository basketProductRepository;

    public BasketResponse addProductToBasket(Integer basketId, Integer productId, Integer amount){
        Basket basket;
        if (basketId==null) {
            basket = new Basket();
            basketRepository.save(basket);
        }else {
            Optional<Basket> optionalBasket = basketRepository.findById(basketId);
            if(!optionalBasket.isPresent())
                return new BasketResponse("savat topilmadi", false, null);
            basket= optionalBasket.get();
        }

        Optional<Product> optionalProduct = productRepository.findById(productId);
        if(!optionalProduct.isPresent())
            return new BasketResponse("product topilmadi", false, null);

        Product product = optionalProduct.get();

        List<BasketProduct> basketProducts = basket.getBasketProducts();
        BasketProduct basketProduct=new BasketProduct(basket, product, amount);
        basketProductRepository.save(basketProduct);
        boolean add = basketProducts.add(basketProduct);

        if(add)
            return new BasketResponse("product savatga qo'shildi", true, basket.getId());
        else
            return new BasketResponse("xatolik", false, null);
    }

    public List<Basket> getAllBaskets(){
        return basketRepository.findAll();
    }

    public Basket getBasketById(Integer id){
        return basketRepository.findById(id).orElse(null);
    }

    public ApiResponse deleteProductFromBasket(Integer basketId, Integer basketProductId){
        Optional<Basket> optionalBasket = basketRepository.findById(basketId);
        if(!optionalBasket.isPresent())
            return new ApiResponse("savat topilmadi", false);

        Optional<BasketProduct> optionalBasketProduct = basketProductRepository.findById(basketProductId);
        if(!optionalBasketProduct.isPresent())
            return new ApiResponse("product topilmadi", false);

        BasketProduct basketProduct=optionalBasketProduct.get();
        Basket basket= optionalBasket.get();
        List<BasketProduct> basketProducts = basket.getBasketProducts();
        for (BasketProduct product : basketProducts) {
            basketProductRepository.delete(product);
        }

        if(!basketProducts.contains(basketProduct))
            return new ApiResponse("savatda bunaqa product yo'q", false);

        boolean remove = basketProducts.remove(basketProduct);

        if(remove)
            return new ApiResponse("product savatdan o'chirildi", true);
        else
            return new ApiResponse("xatolik", false);
    }

    public ApiResponse createOrder(Integer basketId, BasketDto basketDto){
        Optional<Basket> optionalBasket = basketRepository.findById(basketId);
        if(!optionalBasket.isPresent())
            return new ApiResponse("savat topilmadi", false);

        try {
            Basket basket = optionalBasket.get();
            basket.setPhoneNumber(basketDto.getPhoneNumber());
            basket.setEmail(basketDto.getEmail());
            basket.setName(basketDto.getName());
            basket.setAddress(basketDto.getAddress());
            basket.setGeolocation(basketDto.getGeolocation());
            basket.setBasket(basketDto.isBasket());
            basketRepository.save(basket);
            return new ApiResponse("buyurtma saqlandi", true);
        }catch (Exception e) {
            return new ApiResponse(e.getMessage(), false);
        }
    }

    public ApiResponse deleteBasket(Integer basketId){
        Optional<Basket> optionalBasket = basketRepository.findById(basketId);
        if(!optionalBasket.isPresent())
            return new ApiResponse("savat topilmadi", false);

        List<BasketProduct> basketProductList = basketProductRepository.findAllByBasket(optionalBasket.get());
        for (BasketProduct basketProduct : basketProductList) {
            basketProductRepository.delete(basketProduct);
        }



        try{
            basketRepository.deleteById(basketId);
            return new ApiResponse("savat o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse(e.getMessage(), false);
        }
    }
}
