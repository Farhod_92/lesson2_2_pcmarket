package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_2_pcmarket.entity.Attachment;
import uzb.farhod.lesson2_2_pcmarket.entity.AttachmentContent;

import java.util.Optional;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Integer> {
    Optional<AttachmentContent> findByAttachment(Attachment attachment);
}
