package uzb.farhod.lesson2_2_pcmarket.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_2_pcmarket.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
