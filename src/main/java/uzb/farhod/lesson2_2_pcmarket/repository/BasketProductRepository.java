package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_2_pcmarket.entity.Attachment;
import uzb.farhod.lesson2_2_pcmarket.entity.Basket;
import uzb.farhod.lesson2_2_pcmarket.entity.BasketProduct;
import uzb.farhod.lesson2_2_pcmarket.entity.Product;

import java.util.List;
import java.util.Optional;

public interface BasketProductRepository extends JpaRepository<BasketProduct, Integer> {
   // Optional<BasketProduct> findByBasket(Basket basket);

    List<BasketProduct> findAllByBasket(Basket basket);
}
