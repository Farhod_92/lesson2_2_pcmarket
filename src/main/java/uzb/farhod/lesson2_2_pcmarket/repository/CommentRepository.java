package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_2_pcmarket.entity.Comment;
import uzb.farhod.lesson2_2_pcmarket.entity.Product;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
}
