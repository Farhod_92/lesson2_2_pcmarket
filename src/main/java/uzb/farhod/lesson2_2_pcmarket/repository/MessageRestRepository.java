package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_2_pcmarket.entity.Message;

@RepositoryRestResource(path = "message", collectionResourceRel = "list")
public interface MessageRestRepository extends JpaRepository<Message, Integer> {
}
