package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_2_pcmarket.entity.Category;
import uzb.farhod.lesson2_2_pcmarket.entity.Property;

@RepositoryRestResource(path = "category", collectionResourceRel = "list")
public interface CategoryRestRepository extends JpaRepository<Category, Integer> {
}
