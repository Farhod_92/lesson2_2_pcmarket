package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_2_pcmarket.entity.Character;
import uzb.farhod.lesson2_2_pcmarket.entity.Property;

import java.util.Optional;

@RepositoryRestResource(path = "character", collectionResourceRel = "list")
public interface CharacterRestRepository extends JpaRepository<Character, Integer> {

    Optional<Character> findByName(String name);
}
