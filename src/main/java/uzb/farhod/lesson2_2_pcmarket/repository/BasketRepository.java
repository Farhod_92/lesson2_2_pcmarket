package uzb.farhod.lesson2_2_pcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_2_pcmarket.entity.Basket;
import uzb.farhod.lesson2_2_pcmarket.entity.Comment;

public interface BasketRepository extends JpaRepository<Basket, Integer> {
}
