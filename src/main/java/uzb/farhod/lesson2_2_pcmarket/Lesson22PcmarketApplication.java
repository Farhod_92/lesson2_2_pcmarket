package uzb.farhod.lesson2_2_pcmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson22PcmarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson22PcmarketApplication.class, args);
    }

}
