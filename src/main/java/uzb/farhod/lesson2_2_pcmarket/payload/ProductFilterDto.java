package uzb.farhod.lesson2_2_pcmarket.payload;

import lombok.Getter;
import uzb.farhod.lesson2_2_pcmarket.entity.Category;
import uzb.farhod.lesson2_2_pcmarket.entity.Character;
import uzb.farhod.lesson2_2_pcmarket.entity.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
public class ProductFilterDto {
    private float minPrice;
    private float maxPrice;
    //private Map<String,String> characterProperties;
    List<Integer> propertyIds;
}
