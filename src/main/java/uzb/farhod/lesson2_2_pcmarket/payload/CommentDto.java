package uzb.farhod.lesson2_2_pcmarket.payload;

import lombok.Getter;
import uzb.farhod.lesson2_2_pcmarket.entity.Attachment;

@Getter
public class CommentDto {

    private String title;

    private String text;

}
