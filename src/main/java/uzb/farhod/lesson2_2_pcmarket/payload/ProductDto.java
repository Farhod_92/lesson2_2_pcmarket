package uzb.farhod.lesson2_2_pcmarket.payload;

import lombok.Getter;
import uzb.farhod.lesson2_2_pcmarket.entity.Attachment;
import uzb.farhod.lesson2_2_pcmarket.entity.Category;
import uzb.farhod.lesson2_2_pcmarket.entity.Character;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
public class ProductDto {

    private Integer categoryId;

    private String name;
    private float price;
    private String warranty;
    private boolean inStock;
    private boolean saleOnlyInComplete;

}
