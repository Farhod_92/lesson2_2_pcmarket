package uzb.farhod.lesson2_2_pcmarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson2_2_pcmarket.entity.Basket;
import uzb.farhod.lesson2_2_pcmarket.payload.ApiResponse;
import uzb.farhod.lesson2_2_pcmarket.payload.BasketDto;
import uzb.farhod.lesson2_2_pcmarket.payload.BasketResponse;
import uzb.farhod.lesson2_2_pcmarket.service.BasketService;

import java.util.List;

@RestController
@RequestMapping("/api/basket")
public class BasketController {
    @Autowired
    private BasketService basketService;

    @PostMapping("/product")
    public ResponseEntity<?> addProductToBasket(@RequestParam(required = false) Integer basketId, @RequestParam Integer productId,  @RequestParam(defaultValue = "1") Integer amount){
        BasketResponse basketResponse = basketService.addProductToBasket(basketId, productId, amount);
        return ResponseEntity.status(basketResponse.isSuccess()?201:409).body(basketResponse);
    }

    @PreAuthorize(value = "hasAnyRole('SUPER_ADMIN','OPERATOR')")
    @GetMapping
    public ResponseEntity getAllBaskets(){
        List<Basket> allBaskets = basketService.getAllBaskets();
        return ResponseEntity.status(allBaskets.isEmpty()?204:200).body(allBaskets);
    }
    @GetMapping("/{id}")
    public ResponseEntity getBasketById(@PathVariable Integer id){
        Basket basket = basketService.getBasketById(id);
        return ResponseEntity.status(basket==null?204:200).body(basket);
    }

    @DeleteMapping("/product")
    public ResponseEntity<?> deleteProductFromBasket(@RequestParam Integer basketId, @RequestParam Integer productId){
        ApiResponse apiResponse =basketService.deleteProductFromBasket(basketId,productId);
        return ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

    @PostMapping("/{basketId}")
    public ResponseEntity<?> createOrder(@PathVariable Integer basketId, @RequestBody BasketDto basketDto){
        ApiResponse apiResponse = basketService.createOrder(basketId, basketDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAnyRole('SUPER_ADMIN','OPERATOR')")
    @DeleteMapping("/{basketId}")
    public ResponseEntity<?> deleteBasket(@PathVariable Integer basketId){
        ApiResponse apiResponse = basketService.deleteBasket(basketId);
        return ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);

    }

}
