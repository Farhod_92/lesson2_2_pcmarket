package uzb.farhod.lesson2_2_pcmarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uzb.farhod.lesson2_2_pcmarket.entity.Comment;
import uzb.farhod.lesson2_2_pcmarket.payload.ApiResponse;
import uzb.farhod.lesson2_2_pcmarket.service.AttachmentService;
import uzb.farhod.lesson2_2_pcmarket.service.CommentService;


import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private AttachmentService attachmentService;

    @PostMapping
    public ResponseEntity<?> addProductToBasket(@RequestParam String title, @RequestParam String text, @RequestParam MultipartFile file){
        ApiResponse apiResponse=commentService.add(title, text, file);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getAll(){
        List<Comment> all = commentService.getAll();
        return ResponseEntity.status(all.isEmpty()?409:200).body(all);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Integer id){
        Comment byId = commentService.getById(id);
        return ResponseEntity.status(byId==null?409:200).body(byId);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> addProductToBasket(@PathVariable Integer id, @RequestParam String title, @RequestParam String text, @RequestParam MultipartFile file){
        ApiResponse edit = commentService.edit(id, title, text, file);
        return ResponseEntity.status(edit.isSuccess()?202:409).body(edit);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id){
        ApiResponse delete = commentService.delete(id);
        return ResponseEntity.status(delete.isSuccess()?204:409).body(delete);
    }

    @GetMapping("/photo")
    public  HttpEntity<?> getPhoto(@RequestParam("attachmentId") Integer attachmentId){
        return attachmentService.get(attachmentId);
    }


}
